url=https://olegfiksel.gitlab.io/aquasec-microscanner/microscanner
output_file=/tmp/microscanner
fetch_command=0
DEBUG=0
if command -v curl 2>&1 >>/dev/null; then fetch_command="curl -s -o ${output_file} $url"; fi
if command -v wget 2>&1 >>/dev/null; then fetch_command="wget -q -O ${output_file} $url"; fi
if [[ "${fetch_command}" == 0 ]]; then echo "Can't find curl or wget!"; exit 1; fi
if [[ "${DEBUG}" != 0 ]]; then echo ${fetch_command}; fi

# TODO: install ce-certificates depending on a distro
apk --update add ca-certificates 2>&1 >>/dev/null
${fetch_command} 2>&1 >>/dev/null && \
  chmod +x ${output_file} && \
  ${output_file} $@
  exit $?
